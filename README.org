#+TITLE: Algorytmy knapsack sprawozdanie
#+AUTHOR: Adam Salwowski, Oskar Wyłucki

* Opis
Program zawiera w sobie 3 algorytmy rozwiązujące problem plecakowy
1. bruteforce
2. dynamic
3. fptas-dynamic
* Jak użyć programu
: pip3 install -r requirements.txt
: python3 ./main.py --help
