# Table of Contents

1.  [Opis](#org252a6ed)
2.  [Jak użyć programu](#orgb8dc679)



<a id="org252a6ed"></a>

# Opis

Program zawiera w sobie 3 algorytmy rozwiązujące problem plecakowy

1.  bruteforce
2.  dynamic
3.  fptas-dynamic


<a id="orgb8dc679"></a>

# Jak użyć programu

    pip3 install -r requirements.txt
    python3 ./main.py --help
