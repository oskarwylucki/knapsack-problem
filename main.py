import time
from pprint import pprint

from plot import create_plot
from src.knapsack_bruteforce import knapsack_bruteforce
from src.knapsack_dynamic import knapsack_backwards, knapsack_dynamic
from src.knapsack_fptas import knapsack_fptas, knapsack_fptas_backwards
from src.parser import CAPACITY, NUMBER_OF_ITEMS, PLOT, VALUES, WEIGHTS

fptas_functions = [
    knapsack_fptas,
    knapsack_fptas_backwards,
]
all_functions = [
    *fptas_functions,
    knapsack_dynamic,
    knapsack_backwards,
]

if NUMBER_OF_ITEMS > 20:
    print(f"{knapsack_bruteforce.__name__} will take ages! Skipping...")
else:
    all_functions.append(knapsack_bruteforce)


def separator():
    print()
    print(75 * "=")


def show_cli():
    for func in all_functions:
        separator()
        print(func)
        start_time = time.time()
        max_value, rest = func(VALUES, WEIGHTS, CAPACITY)
        end_time = time.time()
        print("Maximum value:", max_value)
        if NUMBER_OF_ITEMS < 20:
            pprint(rest)

        print(f"Execution time: {end_time - start_time} seconds")


def show_plot():
    create_plot(VALUES, WEIGHTS, CAPACITY, all_functions)


def main() -> None:
    if PLOT:
        show_plot()
    else:
        show_cli()


if __name__ == "__main__":
    main()
