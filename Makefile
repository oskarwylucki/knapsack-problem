all: run

run:
	python3 ./main.py


pre-commit:
	pre-commit install
	pre-commit run --all-files
