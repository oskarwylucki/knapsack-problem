def calculate_value_weight(
    values: list[int], weights: list[int], binary_combination: tuple[int]
) -> tuple[int, int]:
    value: int = 0
    weight: int = 0
    for val, wei, bin_val in zip(values, weights, binary_combination):
        if bin_val == 1:
            value += val
            weight += wei
    return value, weight


def int2bin(number: int) -> str:
    return bin(number)[2:]


def bin2tuple(binary_value: str) -> tuple[int]:
    return tuple(map(int, binary_value))


def knapsack_bruteforce(
    values: list[int], weights: list[int], capacity: int, *args, **kwargs
) -> tuple[int, tuple[int]]:
    num_items: int = len(values)
    num_of_possible_comb: int = 2**num_items
    max_value: int = 0
    optimal_binary_combination: tuple = ()

    for i in range(num_of_possible_comb):
        binary_value: str = int2bin(i).zfill(num_items)
        binary_combination: tuple[int] = bin2tuple(binary_value)

        value, weight = calculate_value_weight(values, weights, binary_combination)
        if weight <= capacity:
            if value > max_value:
                max_value = value
                optimal_binary_combination = binary_combination
    return max_value, optimal_binary_combination
