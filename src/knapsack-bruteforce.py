import random
import time

def calculate_value(values, weights, combination):
    value = 0
    weight = 0
    for i in range(len(combination)):
        if combination[i] == 1:
            value += values[i]
            weight += weights[i]
    return (value, weight)

def knapsack_bruteforce(values, weights, knapsack_capacity):
    num_items = len(values)
    max_value = 0
    optimal_combination = ()

    for i in range(2**num_items):
        combination = tuple(map(int, bin(i)[2:].zfill(num_items)))
        if calculate_value(values, weights, combination)[1] <= knapsack_capacity:
            value, _ = calculate_value(values, weights, combination)
            if value > max_value:
                max_value = value
                optimal_combination = combination

    return optimal_combination, max_value

def generate_data(size):
    values = [random.randint(1, 100) for _ in range(size)]
    weights = [random.randint(1, 50) for _ in range(size)]
    knapsack_capacity = random.randint(50, 200)
    return values, weights, knapsack_capacity

def main():
    data_sets = [generate_data(5)]
    print(data_sets)

    for idx, data_set in enumerate(data_sets):
        print(f"Data Set {idx + 1}:")
        values, weights, knapsack_capacity = data_set

        start_time = time.time()
        optimal_combination, max_value = knapsack_bruteforce(values, weights, knapsack_capacity)
        end_time = time.time()

        print("Optimal combination of items:", optimal_combination)
        print("Maximum value:", max_value)
        print(f"Execution time: {end_time - start_time} seconds")
        print()

if __name__ == "__main__":
    main()
