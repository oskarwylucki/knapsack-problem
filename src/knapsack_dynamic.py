def knapsack_dynamic(
    values: list[int], weights: list[int], capacity: int, *args, **kwargs
) -> tuple[int, list[list[int]]]:
    num_items = len(values)
    dp = [[0 for _ in range(capacity + 1)] for _ in range(num_items + 1)]

    for i in range(1, num_items + 1):
        for w in range(1, capacity + 1):
            prev_weight = weights[i - 1]
            prev_value = values[i - 1]
            if prev_weight <= w:
                dp[i][w] = max(dp[i - 1][w], dp[i - 1][w - prev_weight] + prev_value)
            else:
                dp[i][w] = dp[i - 1][w]

    max_value: int = dp[num_items][capacity]
    return max_value, dp


def knapsack_backwards(
    values: list[int], weights: list[int], capacity: int, *args, **kwargs
) -> tuple[int, list[int]]:
    dp: list[int] = [0] * (capacity + 1)

    for val, wei in zip(values, weights):
        for i in range(capacity, wei - 1, -1):
            dp[i] = max(dp[i], dp[i - wei] + val)

    return dp[capacity], dp
