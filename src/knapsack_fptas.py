from src.knapsack_dynamic import knapsack_backwards, knapsack_dynamic
from src.parser import EPSILON


def knapsack_fptas(
    values, weights, capacity, epsilon=EPSILON, func=knapsack_dynamic, *args, **kwargs
):
    n = len(values)
    if n == 0:
        return 0, []

    P = max(values)
    K = epsilon * P / n
    values_scaled = [int(v / K) for v in values]
    result, dp = func(values_scaled, weights, capacity)
    approx_result = int(result * K)

    # OPT_SOLUTION, OPT_DP = knapsack_dynamic(values, weights, capacity)
    # if approx_result >= (1 - epsilon) * OPT_SOLUTION:
    #     print("Lemma: approx_result >= (1 - epsilon) * OPT_SOLUTION")
    #     print("approx_result:", approx_result)
    #     print("(1 - epsilon) * OPT_SOLUTION):", (1 - epsilon) * OPT_SOLUTION)
    # else:
    #     print("SOMETHING WRONG")

    return approx_result, dp


def knapsack_fptas_backwards(
    values, weights, capacity, epsilon=EPSILON, func=knapsack_backwards
):
    return knapsack_fptas(values, weights, capacity, epsilon, func)
