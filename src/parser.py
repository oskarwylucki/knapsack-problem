import argparse
import random
from pprint import pprint


def float_range_0_to_1(value: float) -> float | argparse.ArgumentTypeError:
    fvalue = float(value)
    if fvalue <= 0.0 or fvalue > 1.0:
        raise argparse.ArgumentTypeError(f"{value} is not in range (0, 1]")
    return fvalue


parser = argparse.ArgumentParser(description="idk")
parser.add_argument(
    "-v",
    "--values",
    nargs="+",
    type=int,
    help="set values",
)
parser.add_argument(
    "-w",
    "--weights",
    nargs="+",
    type=int,
    help="specify thread/s",
)
parser.add_argument(
    "-n",
    "--number-of-items",
    type=int,
    default=10,
    help="set number of items",
)
parser.add_argument(
    "-s",
    "--seed",
    type=int,
    help="specify seed",
)
parser.add_argument(
    "-e",
    "--epsilon",
    type=float_range_0_to_1,
    default=0.5,
    help="specify epsilon for FPTAS",
)
parser.add_argument(
    "-c",
    "--capacity",
    type=int,
    default=10,
    help="specify capacity of knapsack",
)
parser.add_argument(
    "-m",
    "--max-value",
    type=int,
    default=1000,
    help="maximal possible value",
)
parser.add_argument(
    "-M",
    "--max-weight",
    type=int,
    default=100,
    help="maximal possible weight",
)
parser.add_argument(
    "-p",
    "--plot",
    action="store_true",
    help="show plot",
)
args = parser.parse_args()


if args.seed:
    random.seed(args.seed)


args.values = args.values or [
    random.randint(1, args.max_value) for _ in range(args.number_of_items)
]
args.weights = args.weights or [
    random.randint(1, args.max_weight) for _ in range(args.number_of_items)
]
args.number_of_items = len(args.values)

NUMBER_OF_ITEMS: int = args.number_of_items
VALUES: list[int] = args.values
WEIGHTS: list[int] = args.weights
WEIGHTS: list[int] = args.weights
EPSILON: float = args.epsilon
CAPACITY: int = args.capacity
PLOT: bool = args.plot

if NUMBER_OF_ITEMS < 50:
    pprint(args.__dict__)
