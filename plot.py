import time

import matplotlib.pyplot as plt


def calculate_eps_times(values, weights, capacity, func):
    plot_epsilons = []
    plot_times = []
    epsilons = [i / 10 for i in range(1, 11)]
    for epsilon in epsilons:
        start_time = time.time()
        result, dp = func(values, weights, capacity, epsilon)
        end_time = time.time()
        plot_epsilons.append(epsilon)
        plot_times.append(end_time - start_time)

    return (
        plot_epsilons,
        plot_times,
    )


def create_plot(values, weights, capacity, functions):
    fig, ax = plt.subplots(figsize=(8, 5), sharey=True)
    fig.suptitle(
        f"Execution Time for Different Algorithms with Varying Epsilons"  # noqa 501
    )
    for func in functions:
        eps, times = calculate_eps_times(values, weights, capacity, func)
        ax.plot(eps, times, label=func.__name__)

    ax.set_xlabel("Epsilon")
    ax.set_ylabel("Time of execution")
    ax.set_title(f"Number of items: {len(values)}, capacity: {capacity}")
    ax.legend()

    plt.tight_layout()
    plt.show()
